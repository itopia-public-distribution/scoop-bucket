# scoop-bucket



## Prerequisities

Have scoop installed on your local machine, cf. [https://scoop.sh](https://scoop.sh) for instructions

## Add the itopia bucket to your local scoop

Open a powershell and execute the following command:

```bash
scoop bucket add itopia https://gitlab.com/isynth-io/scoop-bucket.git
```


## Install an itopia tool

Now you can easily install all tools published by itopia in this scoop bucket. For example you can install the isynth CLI with the following command:

```bash 
scoop install isynth-cli
```

## Update an itopia tool

You can check if a new version of your installed tool has been published:

```bash 
# update the metadata in case your scoop is outdated
scoop update
# check for new versions 
scoop status
```

You can update an app (e.g. isynth-cli) by running the following command:

```bash
scoop update isynth-cli
```